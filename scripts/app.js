var App = angular.module('drag-and-drop', ['ngDragDrop']);

App.controller('oneCtrl', function($scope, $timeout) {
  $scope.list1 = [];

  $scope.list2 = [];

  $scope.list3 = [];

  $scope.list4 = [];



  $scope.list5 = [
    { 'title': 'Item 1', 'class': 'btn btn-info btn-draggable', 'drag': true },
    { 'title': 'Item 2', 'class': 'btn btn-success btn-draggable','drag': true },
    { 'title': 'Item 3', 'class': 'btn btn-danger btn-draggable','drag': true }
  ];
});
